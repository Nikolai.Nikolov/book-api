import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { BASE_URL } from '../../common/constants';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';

const EditBook = ({ history, location, match}) => {

    const id = match.params.id;
    console.log(id);
    const bookId = location.pathname.slice(7, 8);
    const [newBook, setBook] = useState({
        title: '',
        author: '',
        description: '',
        picture: '',
        link: '',
    });

    const [error, setError] = useState(null);


    const updateBookContent = (prop, value) => setBook({
        ...newBook,
        [prop]: value,
    });

    useEffect(() => {
        fetch(`${BASE_URL}/books/${id}`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(response => response.json())
            .then(book => {
                setBook({
                    title: book.title,
                    author: book.author,
                    description: book.description,
                    poster: book.poster,
                    previewLink: book.previewLink,
                    datePublished: book.datePublished,
                })
            })
    }, [id])

    const updateBook = ({ title, author, description }) => {

        fetch(`${BASE_URL}/books/${id}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
            body: JSON.stringify({
                title,
                author,
                description,
            }),
        })
            .then(response => response.json())
            .then(book => {
                if (!book) {
                    throw new Error(book.message);
                };
                history.push(`/books/${bookId}`);
            })
            .catch(error => setError(error.message));
    };

    return (
        <div className="center">
            <label htmlFor="edit-book-title">Title: </label>
            <br></br>
            <input type="text" id="edit-book-title" value={newBook.title} onChange={e => updateBookContent('title', e.target.value)} /><br /><br />
            <label htmlFor="edit-book-author">Author: </label>
            <br></br>
            <input type="text" id="edit-book-author" value={newBook.author} onChange={e => updateBookContent('author', e.target.value)} /><br /><br />
            <label htmlFor="edit-book-description">Description: </label> <br></br>
            <textarea value={newBook.description} onChange={e => updateBookContent('description', e.target.value)} /><br /><br />
            <SimpleButton onClick={() => updateBook(newBook)}>Update</SimpleButton>
            <SimpleButton onClick={() => history.push(`/books/${bookId}`)}>Back</SimpleButton>
        </div>
    )

};

EditBook.propTypes = {
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
};

export default withRouter(EditBook);