import React, { useContext } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import './Navigation.css';
import AuthContext from '../../providers/AuthContext';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';

const Navigation = (props) => {
  const { isLoggedIn, setLoginState } = useContext(AuthContext);
  const history = props.history;

  const logout = (e) => {
    e.preventDefault();
    localStorage.removeItem('token');

    setLoginState({
      isLoggedIn: false,
      user: null,
    });
    history.push('/')
  }

  return (

    <div className="leftmenu">
      <div className="nav">
        <ul>
          <li><NavLink to="/" className="link">Home</NavLink></li>
          {isLoggedIn
            ? (
              <>
                <li><NavLink to="/books" className="link">All books</NavLink></li>
                <li><NavLink to="/create" className="link">Create book</NavLink></li>
                <li><NavLink to="/users" className="link">Top Users</NavLink></li>
                <li><NavLink to="/profile" className="link">Profile</NavLink></li>
                <SimpleButton onClick={logout}>Logout</SimpleButton>
              </>
            )
            : <>
              <li><NavLink to="/register">Register</NavLink></li>
              <li><NavLink to="/login">Login</NavLink><br /></li>
            </>}

        </ul>
      </div>
    </div>

  )
};

export default withRouter(Navigation);

