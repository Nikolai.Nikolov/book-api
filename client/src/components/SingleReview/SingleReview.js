import React, { useContext } from 'react';
import propTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';


const SingleReview = (props) => {

  const history = props.history;
  const review = props.review;
  const book = props.book;
 

  const  loggedUser = useContext(AuthContext);
  const isCreator = loggedUser.id === review.createdBy.id ? true : false;
  const deleteReview = props.deleteReview;




  return (
    <div className="center">
      <hr></hr>
      <h4>Date: {review.date}</h4><br />
      <img
        src={review.createdBy.picture}
        width="130" height="121"
         />
      <h4>by {review.createdBy.displayName}</h4>
      <h1>{review.content}</h1>

      <div className='reactions'>
        <div className='likes'>
          <h3>Dislikes</h3>
          <h3>{review.Dislikes}</h3>
        </div>
        <div className='dislikes'>
          <h3>Likes</h3>
          <h3>{review.Likes}</h3>
        </div>
      </div>

      {
        isCreator || loggedUser.role === "Admin"
          ?
          <div >
            <SimpleButton onClick={() => history.push(`/books/${book.id}/reviews/${review.id}`)} value='false'>Edit</SimpleButton>
            <SimpleButton onClick={(e) => (e.preventDefault(), deleteReview(review.id))}>Delete</SimpleButton>
          </div>
          : null
      }
      <hr></hr>
    </div>
  )
};

SingleReview.propTypes = {
  review: propTypes.object.isRequired,
};

export default withRouter(SingleReview);

