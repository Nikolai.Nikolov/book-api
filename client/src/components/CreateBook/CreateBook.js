import React, { useState } from 'react';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';
import { BASE_URL } from '../../common/constants';

const CreateBook = (props) => {
  const history = props.history;

  const [title, setTitle] = useState('');
  const [author, setAuthor] = useState('');
  const [description, setDescription] = useState('');
  const [error, setError] = useState(null);

  const addBook = (title, author, description) => {
    if (!title) {
      return alert(`No book title provided!`);
    }
    if (!author) {
      return alert(`No book author provided!`);
    }
    if (!description) {
      return alert(`No book description provided!`);
    }
    fetch(`${BASE_URL}/books`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ title, author, description }),
    })
      .then(r => r.json())
      .then(book => {
        if (book.error) {
          throw new Error(book.message);
        }

        history.push('/books');
      })
      .catch(error => setError(error.message));
  };

  
  return (
    <div className="center">
      <h1>Create a book</h1><br />
      <label htmlFor="book-text">Book title:</label><br></br>
      <input id="book-title" type="text" onChange={e => setTitle(e.target.value)}></input><br /><br />
      <label htmlFor="book-author">Book author:</label><br></br>
      <input id="book-author" type="text" onChange={e => setAuthor(e.target.value)}></input><br /><br />
      <label htmlFor="book-description">Book description:</label><br></br>
      <textarea id="review-content" rows="5" cols="50" type="textarea" onChange={e => setDescription(e.target.value)}></textarea><br /><br />
      <SimpleButton onClick={() => addBook(title, author, description)}>Create</SimpleButton>
      <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
    </div>
  );
}

export default CreateBook;

