import React from 'react'

const Header = () =>  {
    return (
        <div id="header">
        <div className="title">
          <h1>Book Library</h1>
          <h3>Welcome to our amazing book-api</h3>
        </div>
      </div>
    )
}

export default Header;