import React, { useState, useEffect } from 'react';
import SingleBook from '../SingleBook/SingleBook';
import { BASE_URL } from '../../common/constants';

const AllBooks = (props) => {
  const [books, setBooks] = useState([]);
  const [error, setError] = useState(null);
  const booksCount = books.length;


  useEffect(() => {
    fetch(`${BASE_URL}/books`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
      .then(r => r.json())
      .then(books => {
        if (books.error) {
          throw new Error(books.message);
        }

        setBooks(books);
      })
      .catch(error => setError(error.message))
  }, [booksCount]);

  return (
    <div className="center">
      <div >
        {books.map(b => <SingleBook
          key={b.id}
          bookId={b.id}
          book={b} />)}
      </div>
    </div>
  );
}

export default AllBooks;
