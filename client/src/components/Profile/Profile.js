import React, { useState, useContext, useEffect } from 'react';
import { BASE_URL } from '../../common/constants';
import SingleBook from '../SingleBook/SingleBook';
import AuthContext from '../../providers/AuthContext';
import "./Profile.css"

const Profile = () => {
  
  const { loggedUser } = useContext(AuthContext);
  const [user, setUser] = useState({
    borrowedBooks: [],
  });
  const [, setError] = useState(null);
  const id = loggedUser.id;

  useEffect(() => {
    fetch(`${BASE_URL}/users/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
      .then(r => r.json())
      .then(user => {
        if (user.error) {
          throw new Error(user.message);
        }
        setUser({
          borrowedBooks: user.borrowedBooks,
          readingPoints: user.readingPoints,
          picture: user.picture,
        });
      })
      .catch(error => setError(error.message))
  }, [loggedUser.borrowedBooks ]);
  return (
    <div className="center">
      <img className="image"
          src={user.picture}
          width="130" height="121"
          alt="image"/ >
      <h1>Username: {loggedUser.displayName}</h1><br/>
      <h1>Reading Points: {user.readingPoints}</h1><br/>
      <h2>Borrowed Books: </h2> <br/>
        < >
          { user.borrowedBooks.length !== 0 
          ? user.borrowedBooks.map(book => <SingleBook
            key={book.id}
            book={book} />) 
            : <h2>No Borrowed Books</h2>
            }
        </>
    </div>
  );
}

export default Profile;
