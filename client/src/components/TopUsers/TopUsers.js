import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../common/constants';
import SingleUser from './SingleUser';

const TopUsers = (props) => {

  const [error, setError] = useState(null);
  const [users, setUsers] = useState([]);


  useEffect(() => {
    fetch(`${BASE_URL}/users`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
      .then(r => r.json())
      .then(users => {
      if (users.error) {
          throw new Error(users.message);
        }
        setUsers(users);
      })
      .catch(error => setError(error.message))
  }, []);
  
  return (
    <div className="center">
      <div >
        {users.map(u => <SingleUser
          key={u.id}
          userId={u.id}
          picture={u.picture}
          displayName={u.displayName}
          readingPoints={u.readingPoints}
          user={u} />)}
      </div>
    </div>
  );
}

export default TopUsers;
