import React, {useContext} from 'react';
import propTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import "./SingleUse.css"
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';
import AuthContext from '../../providers/AuthContext';

const SingleUser = (props) => {

    const user = props.user;
    const history = props.history;

    const { isLoggedIn, loggedUser, setLoginState } = useContext(AuthContext);

    return (
        <div className="center">
            <hr></hr>
            <Card style={{ width: '15rem' }}>
                <Card.Img className="image" variant="top" src={user.picture} width="100" height="110" style={{ float: "left" }} />
                <Card.Title > <h1>{user.displayName} </h1></Card.Title>
                <Card.Title ><h4>Reading Points: {user.readingPoints}</h4></Card.Title><br />
                {loggedUser.role === 'Admin'
                    ?
                    <SimpleButton onClick={() => history.push(`/admin/users/${user.id}/ban`)} user={user}>Ban user</SimpleButton>
                    : null
                }
            </Card>

        </div>
    )
};


SingleUser.propTypes = {
    user: propTypes.object.isRequired,
};

export default withRouter(SingleUser);
