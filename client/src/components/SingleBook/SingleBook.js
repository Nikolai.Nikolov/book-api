import React, { useState, useContext } from 'react';
import propTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { BASE_URL } from '../../common/constants'
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';
import AuthContext from '../../providers/AuthContext'
import {  Card } from 'react-bootstrap';
import BeautyStars from "beauty-stars";

const SingleBook = (props) => {
  const book = props.book;
  const history = props.history;
  const location = props.location;

  const isProfile = location.pathname.includes('profile');
  const [error, setError] = useState(null);
  const [status, setStatus] = useState(null);

  const { isLoggedIn, loggedUser, setLoginState } = useContext(AuthContext);


  const returnBook = () => {
    fetch(`${BASE_URL}/books/${book.id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`
      },
      body: JSON.stringify({
        id: book.id,
      }),
    })
      .then((response) => response.json())
      .then((book) => {

        setStatus(book.bookStatus === 'Free' ? book.bookStatus.Borrowed : book.bookStatus.Free);
      })
      .catch(error => setError(error.message))
      history.push('/books')
    
  }

  const deleteBook = () => {
    fetch(`${BASE_URL}/books/${book.id}/unlisted`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token') || ''}`
        },
        body: JSON.stringify({
            id: book.id,
        }),
    })
        .then((response) => response.json())
        .then((status) => {
            alert('You have just deleted this book!')
            setStatus(status.bookStatus.Unlisted);
        })
        .catch(error => setError(error.message))
  };

  return (
    <div  className="center">
      <hr></hr>
      <Card style={{ width: '18rem' }}>
      <Card.Title > <h1>{book.title} </h1></Card.Title>
      <Card.Img variant="top" src={book.picture} width="150" height="200" style={{float: "left"}}/>
      <Card.Title > <h3>{book.description}</h3></Card.Title><br />
      <Card.Title > <h4>by {book.author}</h4></Card.Title><br />
      <Card.Title ><h4 >Status: {book.bookStatus === 1 ? 'Free' : 'Borowed'}</h4></Card.Title><br />
      <Card.Title > <h4>Rating:{book.rating}</h4></Card.Title><br />
      <BeautyStars  value={book.rating} size='20' gap='7px' activeColor='#FF8C00'/>
      
      {isProfile
        ? <SimpleButton  onClick={() => returnBook()}>Return book</SimpleButton>
        : <SimpleButton onClick={() => props.history.push(`/books/${book.id}`)}>Review this book</SimpleButton>
      }
      { loggedUser.role === 'Admin'
        ? <>
          <SimpleButton onClick={() => history.push(`/edit/${book.id}`)} book={book}>Edit Book</SimpleButton>
          <SimpleButton onClick={() => deleteBook()}>Delete Book</SimpleButton>
        </>
        : null
      }
      </Card>
      
    </div>
  )
};


SingleBook.propTypes = {
  book: propTypes.object.isRequired,
};

export default withRouter(SingleBook);
