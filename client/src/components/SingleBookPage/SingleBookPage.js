import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import { BASE_URL } from '../../common/constants'
import './Single.css'
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';
import { Card } from 'react-bootstrap';
import BeautyStars from "beauty-stars";

const SingleBookPage = ({ match, history }) => {

    const id = match.params['id'];
    console.log(id);
    const [book, setBook] = useState([]);
    const [error, setError] = useState(null);


    useEffect(() => {
        fetch(`${BASE_URL}/books/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(response => response.json())
            .then(book => {
                if (book.error) {
                    throw new Error(book.message);
                }
                setBook(book);
            })
            .catch(error => setError(error.message));
    }, [id]);

    const borrow = () => {
        fetch(`${BASE_URL}/books/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`
            },
            body: JSON.stringify({
                id: book.id,
            }),
        })
            .then((response) => response.json())
            .then((status) => {

            })
            .catch(error => setError(error.message))
        history.push('/books')
    }
    const giveRate = (value) => {
        fetch(`${BASE_URL}/books/${id}/rating`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`
            },
            body: JSON.stringify({
                stars: value,
            })
        })
            .then((response) => response.json())
            .then((status) => {
                setBook(prevBook => {
                     prevBook.rating = status.rating ;
                     return {...prevBook}
                    })
               
                if (status.message) {
                    alert('you cant double rate a book');
                }
            })
            .catch(error => setError(error.message))
    }
    return (
        <div className="center">
            <br></br>
            <br></br>
            <Card style={{ width: '18rem' }}>
                <Card.Title > <h1>{book.title} </h1></Card.Title>
                <Card.Img className='card-img' variant="top" src={book.picture}  width="180" height="200" style={{ float: "left" }} />
                <Card.Title > <h3>{book.description}</h3></Card.Title><br />
                <Card.Title > <h4>by {book.author}</h4></Card.Title><br />
                <Card.Title ><h4 >Status: {book.bookStatus === 1 ? 'Free' : 'Borowed'}</h4></Card.Title><br />  
                <Card.Title > <h4>Rating:{book.rating}</h4></Card.Title><br />
                <BeautyStars
                    value={book.rating}
                    onChange={value =>  giveRate(value)}
                    size='20px'
                    gap='7px'
                    activeColor='#FF8C00'
                />
                <div className="singleButon">
                    <div className='right'>
                    <SimpleButton  onClick={() => history.push(`/books/${id}/reviews`)}>Show Reviews</SimpleButton>
                    </div>
                    <br></br>
                    <div className='left' >
                    <SimpleButton onClick={() => borrow()}>Borrow</SimpleButton>
                    </div>
                    <br></br>
                </div>

            </Card>
        </div>
    )
}

SingleBookPage.propTypes = {
    match: PropTypes.object.isRequired,
};
export default SingleBookPage;




 

 

