import React, { useState } from 'react';
import PropTypes from 'prop-types';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';
import { BASE_URL } from '../../common/constants';
import { withRouter } from 'react-router-dom';

const BanUser = ({match, history, }) => {
 
  const id = match.params.id;

  const [period, setPeriod] = useState(null);
  const [description, setDescription] = useState('');
  const [error, setError] = useState(null);

  const Ban = (period, description) => {
    if (!period) {
      return alert(`Enter time frame`);
    }
    if (!description) {
      return alert(`Provide description`);
    }
    fetch(`${BASE_URL}/admin/users/${id}/ban`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ period, description }),
    })
      .then(r => r.json())
      .then(ban => {
        if (ban.error) {
          throw new Error(ban.message);
        }
        history.push("/users")
      })
      .catch(error => setError(error.message));
  };

  
  return (
    <div className="center">
      <h1>Ban user</h1><br />
      <label htmlFor="book-text">Ban period</label><br></br>
      <input id="ban-period" type="text" onChange={e => setPeriod(+e.target.value)}></input><br /><br />
      <label htmlFor="ban-description">Ban description:</label><br></br>
      <textarea id="ban-content" rows="5" cols="50" type="textarea" onChange={e => setDescription(e.target.value)}></textarea><br /><br />
      <SimpleButton onClick={() => Ban(period, description)}>Ban this user</SimpleButton>
      <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
    </div>
  );
}
BanUser.propTypes = {
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
};
export default withRouter(BanUser);

