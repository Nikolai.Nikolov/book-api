import React, { useState, useContext } from 'react';
import { BASE_URL } from '../../common/constants';
import AuthContext, { extractUser } from '../../providers/AuthContext';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';

const SignIn = (props) => {
  const history = props.history;
  const location = props.location;

  const {  isLoggedIn, loggedUser, setLoginState } = useContext(AuthContext);

  const [user, setUserObject] = useState({
    username: '',
    password: '',
  });

  const updateUser = (prop, value) => setUserObject({...user, [prop]: value });

  const isLogin = location.pathname.includes('login');
  const login = () => {
    if (!user.username) {
      return alert('Invalid username!');
    }
    if (!user.password) {
      return alert('Invalid password!');
    }

    fetch(`${BASE_URL}/session`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return alert(result.message);
        }
        try {
          setLoginState({isLoggedIn: true, loggedUser: extractUser(result.token)});
        } catch(e) {
          return alert(e.message.constraints);
        }

        localStorage.setItem('token', result.token);
        history.push('/profile');
      })
      .catch(alert); 
  };

  const register = () => {
    if (!user.username) {
      return alert('Invalid username!');
    }
    if (!user.password) {
      return alert('Invalid password!');
    }
    if (user.password !== user.confirmPassword) {
        return alert('Passwords doesn\'t match !');
      }

    fetch(`${BASE_URL}/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        ...user,
        displayName: user.username,
      }),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return alert(result.message);
        }

        history.push('/login');
      })
      .catch(alert);
  };

  return (
    <div className="center">
      <h1>{isLogin ? 'Login' : 'Register'}</h1>
      <br></br>
      <label htmlFor="input-username">Username:</label>
      <br></br>
      <input type="text" id="input-username" placeholder="Type your Username" value={user.username} onChange={(e) => updateUser('username', e.target.value)} /><br />
      <br></br>
      <label htmlFor="input-password">Password: </label>
      <br></br>
      <input type="password" id="input-password" placeholder="Type your password" value={user.password} onChange={(e) => updateUser('password', e.target.value)} /><br /><br />
      { isLogin
        ? <div>
        <SimpleButton onClick={login}>Login</SimpleButton>    <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
        </div>
        : <div>
        <label htmlFor="input-confirmPass">Confirm Password:</label> <br></br>
        <input type="password" id="input-confirmPass" placeholder="Confirm your password" value={user.confirmPassword} onChange={(e) => updateUser('confirmPassword', e.target.value)} /><br /><br />
        <SimpleButton onClick={register}>Register</SimpleButton>
        <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
        </div>}
    </div>
  );
};

export default SignIn;