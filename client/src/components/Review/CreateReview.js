import React, { useState } from 'react';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';
import { BASE_URL } from '../../common/constants';

const CreateReview = (props) => {
  const history = props.history;

  const [content, setContent] = useState('');

  const [error, setError] = useState(null);
  const match = props.match;
  
  const addReview = (content) => {
    if (!content) {
      return alert(`No review content provided`);
    }

    fetch(`${BASE_URL}/reviews/books/${match.params.id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({ content }),
    })
      .then(r => r.json())
      .then(review => {
        if (review.error) {
          throw new Error(review.message);
        }
        history.push(`/books/${match.params.id}/reviews`);
      })
      .catch(error => setError(error.message));
  };

  return (
    <div className='center'>
      <h1>Create a review</h1><br />
      <label htmlFor="review-content"><b>Content: </b></label> <br></br>
      <textarea id="review-content" rows="5" cols="50" type="textarea" onChange={e => setContent(e.target.value)}></textarea><br /><br />
      <SimpleButton onClick={() => addReview(content)}>Create</SimpleButton>
      <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
    </div>
  );
}

export default CreateReview;