import React, { useState, useEffect, useContext } from 'react';
import { BASE_URL } from '../../common/constants';
import PropTypes from 'prop-types';
import AuthContext from '../../providers/AuthContext';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';
import { withRouter } from 'react-router-dom';

const EditReview = ({ history, location, match}) => {
  const id = match.params.id;

  const bookId = location.pathname.slice(7, 8);
  const [review, setReview] = useState({
    content: '',
  });
  const [error, setError] = useState(null);

  const { isLoggedIn, loggedUser, setLoginState } = useContext(AuthContext);

  const updateReviewContent = (content) => setReview({
    ...review,
    content,
  });

  const updateReview = () => {
    fetch(`${BASE_URL}/reviews/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify(review),
    })
      .then(response => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        };
        history.push(`/books/${bookId}/reviews`);
      })
      .catch(error => setError(error.message));
  };

  return (
    <div className="center">
      <label htmlFor="edit-review-content">Content: </label><input type="text" id="edit-review-content" value={review.content} onChange={e => updateReviewContent(e.target.value)} /><br /><br />
      <SimpleButton onClick={updateReview}>Update</SimpleButton>
      <SimpleButton onClick={() => history.push(`/books/${bookId}/reviews`)}>Back</SimpleButton>
    </div>
  )

};

EditReview.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  //   bookId: PropTypes.number.isRequired,
};

export default withRouter(EditReview);