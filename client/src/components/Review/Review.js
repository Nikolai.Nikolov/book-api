import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../common/constants';
import SingleReview from '../SingleReview/SingleReview';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton'

const Reviews = (props) => {
    const id = props.match.params['id'];
    const [reviews, setReview] = useState([]);
    const [error, setError] = useState(null);

    const [reviewDeleted, setReviewDeleted] = useState({
        isDeleted: false,
    });
    const [doneDeleting, setDeleted] = useState(false);

    useEffect(() => {
        fetch(`${BASE_URL}/books/${id}/reviews`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(r => r.json())
            .then(reviews => {
                if (reviews.error) {
                    throw new Error(reviews.message);
                }
                setReview(reviews);
            })
            .catch(error => setError(error.message))
    }, [id, reviewDeleted]);

    const deleteReview = (id) => {

        fetch(`${BASE_URL}/reviews/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            }
        })
            .then(result => result.json())
            .then(review => {

                if (!review) {
                    alert('review is null');
                }
                setReviewDeleted({ isDeleted: true });
                setDeleted(true);
            })
    }

    

    return (
        <div className="center">
            {reviews.length > 0 ?
                reviews.map(r => <SingleReview
                    key={r.id}
                    book={r.reviewBook}
                    id={r.id}
                    content={r.content}
                    createdBy={r.createdBy.displayName}
                    Likes={r.Likes}
                    Dislikes={r.Dislikes}
                    deleteReview={deleteReview}
                    review={r}
                    isDeleted={r.isDeleted} />)
                : <h2> "No Reviews yet" </h2>
            }
            <div className="center">
            <SimpleButton onClick={() => props.history.push(`/reviews/books/${id}`)}>Create Review</SimpleButton>
            <SimpleButton onClick={() => props.history.push(`/books/${id}`)}>Back</SimpleButton>
            </div>
            
        </div>
    );
}
export default Reviews;