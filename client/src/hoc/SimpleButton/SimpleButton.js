import React from 'react';
import './SimpleButton.css';

const SimpleButton = props => {
  const onClick = props.onClick;

  return (
    <button className="SimpleButton" onClick={onClick}>
      {props.children}
    </button>
  );
};

export default SimpleButton;
