import React, { useState } from 'react';
import './App.css';
import AllBooks from './components/AllBooks/AllBooks';
import Profile from './components/Profile/Profile';
import Home from './components/Home/Home';
import Navigation from './components/Navigation/Navigation';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import CreateBook from './components/CreateBook/CreateBook';
import SingleBookPage from './components/SingleBookPage/SingleBookPage';
import './images/style.css'
import Header from './components/Header/Header';
import PreFooter from './components/PreFooter/PreFooter';
import Footer from './components/Footer/Footer';
import Reviews from './components/Review/Review';
import AuthContext, { extractUser, getToken } from './providers/AuthContext';
import SignIn from './components/SignIn/SignIn';
import GuardedRoute from './providers/GuardedRoute';
import CreateReview from './components/Review/CreateReview';
import EditReview from './components/Review/EditReview';
import EditBook from './components/EditBook/EditBook';
import TopUsers from './components/TopUsers/TopUsers';
import BanUser from './components/BanUser/BanUser';



const App = () => {

  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    loggedUser: extractUser(getToken())
  });


  return (
    <BrowserRouter>
      <div className="content">
        <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
          <Header></Header>
          <div id="main">
            <Navigation />
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/login" component={SignIn} />
              <Route path="/register" component={SignIn} />
              <GuardedRoute path="/books" auth={authValue.isLoggedIn} exact component={AllBooks} />
              <GuardedRoute path="/books/:id" auth={authValue.isLoggedIn} exact component={SingleBookPage} />
              <GuardedRoute path="/books/:id/reviews" exact auth={authValue.isLoggedIn} component={Reviews} />
              <GuardedRoute path="/reviews/books/:id" exact auth={authValue.isLoggedIn} component={CreateReview} />
              <GuardedRoute path="/books/:id/reviews/:id" exact auth={authValue.isLoggedIn} component={EditReview} />
              <GuardedRoute path="/create" auth={authValue.isLoggedIn} exact component={CreateBook} />
              <GuardedRoute path="/edit/:id" auth={authValue.isLoggedIn} exact component={EditBook} />
              <GuardedRoute path="/users" auth={authValue.isLoggedIn} exact component={TopUsers} />
              <GuardedRoute path="/admin/users/:id/ban" auth={authValue.isLoggedIn} exact component={BanUser} />
              <GuardedRoute path="/profile" auth={authValue.isLoggedIn} component={Profile} />
            </Switch>
          </div>
          <PreFooter></PreFooter>
          <Footer></Footer>
        </AuthContext.Provider>

      </div>
    </BrowserRouter>
  );
}

export default App;
