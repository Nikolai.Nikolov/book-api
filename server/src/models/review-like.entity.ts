import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { ReviewReactionType } from 'src/enums/review-reaction-type';
import { User } from './user.entity';
import { Review } from './reviews.entity';

@Entity('review-like')
export class ReviewLike {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @ManyToOne(type => User, user => user.id)
    user: User;

    @ManyToOne(type => Review, review => review.id)
    review: Review;

    @Column({ type: 'enum', enum: ReviewReactionType})
    reaction: ReviewReactionType;
}
