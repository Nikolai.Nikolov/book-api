import { ReturnBookDTO } from './../dtos/book/return-book.dto';
import { Book } from 'src/models/books.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, } from 'typeorm';
import { Review } from './reviews.entity';
import { BookRate } from './book-rate.entity';
import { ReturnReviewDTO } from 'src/dtos/review/return-review.dto';
import { ReviewLike } from './review-like.entity';
import { ReturnReviewLikeDTO } from 'src/dtos/review-like/return-review-like.dto';
import { UserRole } from 'src/enums/user-role.enum';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', nullable: false, unique: true, length: 20 })
  username: string;

  @Column({ type: 'nvarchar', nullable: false, length: 20 })
  displayName: string;

  @Column({ type: 'nvarchar', nullable: false })
  password: string;

  @Column({ type: 'boolean', default: false })
  // isDeleted: boolean;

  // @Column({ type: 'enum', enum: UserRole, default: UserRole.Basic })
  // role: UserRole;

  // @Column({ type: 'nvarchar', nullable: false, default: 'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png' })
  // picture: string;

  // @OneToMany(
  //   () => Book,
  //   books => books.borrowedBy
  // )
  // borrowedBooks: ReturnBookDTO[];

  @OneToMany(
    () => Review,
    reviews => reviews.createdBy
  )
  postedReviews: ReturnReviewDTO[];

  // @Column({ type: 'int', default: 0 })
  // readingPoints: number;

  // @OneToMany(
  //   () => ReviewLike,
  //   reactionReview => reactionReview.user)
  // reactionsReviews: ReturnReviewLikeDTO[];

  // @OneToMany(
  //   () => BookRate,
  //   rateBook => rateBook.ratedBy
  // )
  // ratedBooks: ReturnBookDTO[];

  // @Column({
  //   nullable: true,
  // })
  // banEndDate: Date;
}
