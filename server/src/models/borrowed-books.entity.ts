import { Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { User } from './user.entity';
import { ReturnBookDTO } from 'src/dtos/book/return-book.dto';
import { Book } from './books.entity';

@Entity('borrow-book')
export class BorrowedBooks {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @ManyToOne(type => User, user => user.id)
    user: User;

    @ManyToOne(type => Book, book => book.id)
    book: Book;
}
