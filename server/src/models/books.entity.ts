import { ReturnReviewDTO } from './../dtos/review/return-review.dto';
import { ReturnBookDTO } from 'src/dtos/book/return-book.dto';
import { BookStatus } from '../enums/book-status';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { User } from './user.entity';
import { Review } from './reviews.entity';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';


@Entity('books')
export class Book {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ type: 'nvarchar', nullable: false, unique: true, length: 200 })
    // title: string;

    // @Column({ type: 'nvarchar', nullable: false, length: 30 })
    // author: string;

    // @Column({ type: 'nvarchar', nullable: false, length: 1000 })
    // description: string;

    // @Column({type: 'nvarchar', nullable: false, default: 'picture'})
    // picture: string;    

    // @Column({type: 'nvarchar', nullable: false, default: 'link'})
    // link: string;

    // @Column({ type: 'enum', enum: BookStatus, default: BookStatus.Free })
    // bookStatus: BookStatus;

    // @ManyToOne(
    //     () => User,
    //     user => user.borrowedBooks
    // )
    // borrowedBy: ReturnUserDTO;

    @OneToMany(
        () => Review,
        bookReview => bookReview.reviewBook
    )
    bookReviews: ReturnReviewDTO[];

    // @Column({ type: 'double', default: 0 })
    // rating: number;
}
