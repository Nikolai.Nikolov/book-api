import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity('banned-user')
export class BannedUser {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ type: 'nvarchar', nullable: false, length: 200 })
    description: string;

    @Column({ type: 'int' })
    period: number;

    @ManyToOne(
        type => User,
        user => user.id
    )
    bannedUser: User;
}
