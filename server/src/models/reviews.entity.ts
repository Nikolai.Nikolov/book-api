import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn } from 'typeorm';
import { User } from './user.entity';
import { Book } from './books.entity';

@Entity('review')
export class Review {

    // @PrimaryGeneratedColumn('increment')
    // id: number;

    // @Column({ type: 'nvarchar', nullable: false, length: 200 })
    // content: string;

    // @CreateDateColumn()
    // date: Date;

    @ManyToOne(
        () => User,
        user => user.id
    )
    createdBy: User;

    @ManyToOne(
        () => Book,
        book => book.bookReviews
    )
    reviewBook: Book;

//     @Column({ type: 'boolean', default: false })
//     isDeleted: boolean;

//     @Column({ type: 'int', default: 0 })
//     Likes: number;

//     @Column({ type: 'int', default: 0 })
//     Dislikes: number;
// }
