import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { Book } from './books.entity';

@Entity('book-rate')
export class BookRate {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ type: 'double' })
    stars: number;

    @ManyToOne(
        type => User,
        user => user.id
    )
    ratedBy: User;

    @ManyToOne(
        type => Book,
        book => book.id
    )
    books: Book;
}
