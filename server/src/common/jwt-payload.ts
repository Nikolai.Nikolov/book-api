import { ReturnBookDTO } from './../dtos/book/return-book.dto';

export class JWTPayload {
    id: number;
    username: string;
    displayName: string;
    role: string;
    readingPoints: number;
    borrowedBooks: ReturnBookDTO[];
}
