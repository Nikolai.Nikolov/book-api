import { ReturnBookDTO } from 'src/dtos/book/return-book.dto';
import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { TransformService } from './transformer.service';
import { Book } from 'src/models/books.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { BookStatus } from 'src/enums/book-status';
import { CreateBookDTO } from 'src/dtos/book/create-book.dto';
import { UpdateBookDTO } from 'src/dtos/book/update-book.dto';
import { ReturnReviewDTO } from 'src/dtos/review/return-review.dto';
import { Review } from 'src/models/reviews.entity';
import { BorrowedBooks } from 'src/models/borrowed-books.entity';
import { BookRate } from 'src/models/book-rate.entity';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';

@Injectable()
export class BooksService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
        @InjectRepository(BorrowedBooks) private readonly borrowedBooksRepository: Repository<BorrowedBooks>,
        @InjectRepository(BookRate) private readonly booksRateRepository: Repository<BookRate>
    ) {
        this.booksRepository = booksRepository;
        this.usersRepository = usersRepository;
        this.transformer = transformer;
    }

    async getAll(): Promise<ReturnBookDTO[]> {
        const books = await this.booksRepository.find({
            where: {
                bookStatus: BookStatus.Free,
            }
        });

        if (!books || books.length === 0) {
            throw new NotFoundException('No book found!');
        }

        return books.map(book => this.transformer.toReturnBookDTO(book));
    }

    async getBookReviews(bookId: number, loggedUserId: number): Promise<ReturnReviewDTO[]> {
        const bookReviews = await this.reviewsRepository.find({
            where: {
                reviewBook: bookId,
                isDeleted: false,
                // tslint:disable-next-line: no-bitwise
                bookStatus: BookStatus.Free | BookStatus.Borrowed,
            },
            relations: ['createdBy', 'reviewBook']
        });

        if (!bookReviews || bookReviews.length === 0) {
            throw new NotFoundException('No reviews found!');
        }
        const bookFound = await this.booksRepository.findOne(bookId, { relations: ['borrowedBy'] });
        const userFound = await this.findOneOrFailUser(loggedUserId);

        if (bookFound.borrowedBy) {
            if (bookFound.borrowedBy.username !== userFound.username) {
                throw new BadRequestException('This reviews can\'t be read as the book is borrowed by someone else!')
            }
        }

        return bookReviews.map(book => this.transformer.toReturnReviewDTO(book));
    }

    async findBook(options: Partial<Book>): Promise<ReturnBookDTO[]> {

        const foundBook = await this.booksRepository.find(options);
        if (!foundBook || foundBook.length === 0) {
            throw new NotFoundException('No such book found!')
        }
        return await foundBook.map(book => this.transformer.toReturnBookDTO(book));
    }

    async getBookById(bookId: number): Promise<ReturnBookDTO> {
        const foundBook = await this.findOneOrFailBook(bookId);
        const newFoundBook = await this.booksRepository.findOne({
            where: {
                id: foundBook.id,
                bookStatus: BookStatus.Free,
            },
        })
        
        return this.transformer.toReturnBookDTO(newFoundBook);
    }

    async create(book: CreateBookDTO): Promise<ReturnMessageDTO> {
        const newBook = this.booksRepository.create(book);
        await this.booksRepository.save(newBook);

        return { message: 'Book created!' };
    }

    async updateBookDetails(bookDTO: Partial<UpdateBookDTO>, bookId: number): Promise<ReturnMessageDTO> {
        const bookToUpdate = await this.findOneOrFailBook(bookId);
        await this.booksRepository.update(bookId, bookDTO);
        if (bookDTO.title === bookToUpdate.title) {
            throw new BadRequestException('Such book title already exists. Title must be unique!')
        }

        return { message: 'Book details updated' };
    }

    async borrowBook(bookId: number, loggedUserId: number): Promise<ReturnMessageDTO> {

        const foundUser = await this.findOneOrFailUser(loggedUserId);
        const bookFound = await this.findOneOrFailBook(bookId);

        bookFound.borrowedBy = foundUser;
        bookFound.bookStatus = BookStatus.Borrowed;
        const borrowedBook = this.borrowedBooksRepository.create();
        borrowedBook.user = foundUser;
        borrowedBook.book = bookFound;
        foundUser.readingPoints++;
        await this.borrowedBooksRepository.save(borrowedBook);

        await this.booksRepository.update(bookId, bookFound);
        await this.usersRepository.update(loggedUserId, foundUser);

        return { message: `Book with ${bookFound.id} id is borrowed by user with id ${foundUser.id}` };
    }


    async returnBook(bookId: number, loggedUserId: number): Promise<ReturnMessageDTO> {
        const foundUser = await this.findOneOrFailUser(loggedUserId);
        const bookFound = await this.booksRepository.findOne(bookId, { relations: ['borrowedBy'] });

        if (!bookFound) {
            throw new BadRequestException('No such book!')
        }

        if (!bookFound.borrowedBy) {
            throw new BadRequestException('That books is not borrowed to be returned');
        }

        if (bookFound.borrowedBy.username === foundUser.username) {
            bookFound.bookStatus = BookStatus.Free;
            bookFound.borrowedBy = new User();
            foundUser.readingPoints++;
        } else {
            throw new BadRequestException('book can\'t be returned by this user');
        }

        await this.booksRepository.update(bookId, bookFound);
        await this.usersRepository.update(loggedUserId, foundUser);

        return { message: `Book with ${bookFound.id} id is now returned by user with id ${foundUser.id}` };
    }

    async delete(bookId: number): Promise<ReturnMessageDTO> {
        const foundBook = await this.booksRepository.findOne(bookId);
        if (!foundBook) {
            throw new NotFoundException(`There is no book with id ${bookId}!`);
        }
        if (foundBook.bookStatus === BookStatus.Unlisted) {
            throw new BadRequestException('That books is already unlisted')
        }

        foundBook.bookStatus = BookStatus.Unlisted;
        await this.booksRepository.save(foundBook);

        return { message: `Book with id ${foundBook.id} is deleted!` }
    }

    async retrieve(bookId: number): Promise<ReturnMessageDTO> {
        const foundBook = await this.booksRepository.findOne(bookId);
        if (!foundBook) {
            throw new NotFoundException(`There is no book with id ${bookId}!`);
        }
        if (foundBook.bookStatus === BookStatus.Free) {
            throw new BadRequestException('That books is already retrieved!')
        }
        foundBook.bookStatus = BookStatus.Free;
        await this.booksRepository.save(foundBook);

        return { message: `Book with id ${foundBook.id} is now retrieved!` }
    }
    private async findOneOrFailBook(bookId: number): Promise<Book> {
        const book = await this.booksRepository.findOne(bookId);
        if (!book) {
            throw new NotFoundException('No such book!')
        }
        if (book.bookStatus === BookStatus.Borrowed) {
            throw new BadRequestException('Book is borrowed')
        }

        return book;
    }

    private async findOneOrFailUser(userId: number): Promise<User> {
        const user = await this.usersRepository.findOne(userId);
        if (!user) {
            throw new NotFoundException('No user!')
        }

        return user;
    }

}
