import { TransformService } from './transformer.service';
import { User } from 'src/models/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { UsersService } from './user.service';
import { BooksService } from './books.service';
import { Book } from 'src/models/books.entity';
import { Review } from 'src/models/reviews.entity';
import { ReviewService } from './review.service';
import { BorrowedBooks } from 'src/models/borrowed-books.entity';
import { ReviewLike } from 'src/models/review-like.entity';
import { RateService } from './rate.service';
import { BookRate } from 'src/models/book-rate.entity';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './../constant/secret';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt-strategy';
import { Token } from 'src/models/token.entity';
import { AdminService } from './admin.service';
import { BannedUser } from 'src/models/banned-user.entity';

@Module({
  imports: [

    TypeOrmModule.forFeature([User, Review, Book, BorrowedBooks, ReviewLike, BookRate, Token, BannedUser]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d',
      }
    })
  ],
  providers: [UsersService, TransformService, BooksService, ReviewService, RateService, AuthService, JwtStrategy, AdminService],
  exports: [UsersService, BooksService, TransformService, ReviewService, RateService, AuthService, AdminService]
})
export class ServicesModule { }
