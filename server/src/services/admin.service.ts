import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { TransformService } from './transformer.service';
import { ReturnReviewDTO } from 'src/dtos/review/return-review.dto';
import { Review } from 'src/models/reviews.entity';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { BannedUser } from 'src/models/banned-user.entity';
import { UserBanDTO } from 'src/dtos/users/user-ban.dto';

@Injectable()
export class AdminService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
        @InjectRepository(BannedUser) private readonly bannedUsersRepository: Repository<BannedUser>,
    ) { }

    async getAll(): Promise<ReturnUserDTO[]> {
        const users = await this.usersRepository.find({
            where: { isDeleted: false },
        });
        if (!users || users.length === 0) {
            throw new NotFoundException('No users  found')
        }
        return users.map(user => this.transformer.toReturnUserDTO(user));
    }

    async getById(userId: number): Promise<ReturnUserDTO> {
        const user = await this.findOneOrFailUser(userId);
        if (!user) {
            throw new NotFoundException('No such user');
        }

        return this.transformer.toReturnUserDTO(user);
    }

    async changeRole(user, userId): Promise<ReturnMessageDTO> {
        const foundUser = await this.findOneOrFailUser(userId);
        if (foundUser.role === user.role) {
            throw new BadRequestException('This user already has this role')
        }
        await this.usersRepository.update(userId, user);

        return { message: 'User role changed' };
    }

    async getUserReviews(userId: number): Promise<ReturnReviewDTO[]> {
        const userFound = await this.usersRepository.findOne(userId, { relations: ['postedReviews'] });
        if (!userFound) {
            throw new NotFoundException('No such user found');
        }
        const userReviews = await this.reviewsRepository.find({ where: { createdBy: userFound, isDeleted: false }, relations: ['reviewBook'] });

        if (!userReviews || userReviews.length === 0) {
            throw new BadRequestException('User have no reviews');
        }

        return await userReviews.map(review => this.transformer.toReturnReviewDTO(review));
    }

    async deleteUser(userId: number): Promise<ReturnMessageDTO> {
        const foundUser = await this.findOneOrFailUser(userId);
        foundUser.isDeleted = true;
        await this.usersRepository.save(foundUser);

        return { message: 'User deleted!' };
    }

    async restoreUser(userId: number): Promise<ReturnMessageDTO> {
        const foundUser = await this.usersRepository.findOne(userId);
        if (!foundUser) {
            throw new NotFoundException('No such user found');
        }
        foundUser.isDeleted = false;
        await this.usersRepository.save(foundUser);

        return { message: 'User restored!' };
    }

    private async findOneOrFailUser(userId: number): Promise<User> {
        const user = await this.usersRepository.findOne(userId, { where: { isDeleted: false } });

        if (!user) {
            throw new NotFoundException('No such user!')
        }

        return user;
    }

    async banUser(userId: number, period: number, banUser: UserBanDTO): Promise<ReturnMessageDTO> {
        const user = await this.findOneOrFailUser(userId);

        user.banEndDate = new Date(Date.now() + period);
        const bannedUser = this.bannedUsersRepository.create();
        bannedUser.bannedUser = user;
        bannedUser.description = banUser.description;
        bannedUser.period = banUser.period;
        await this.bannedUsersRepository.save(bannedUser);
        await this.usersRepository.save(user)

        return { message: 'User Banned' };
    }
}
