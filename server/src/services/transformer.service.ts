import { User } from 'src/models/user.entity';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { ReturnReviewDTO } from 'src/dtos/review/return-review.dto';
import { Review } from 'src/models/reviews.entity';
import { Book } from 'src/models/books.entity';
import { ReturnBookDTO } from 'src/dtos/book/return-book.dto';
import { BookRate } from 'src/models/book-rate.entity';
import { ReturnRateDTO } from 'src/dtos/book-rate/return-book-rate.dto';

export class TransformService {
  toReturnUserDTO(user: User): ReturnUserDTO {
    return {
      id: user.id,
      username: user.username,
      displayName: user.displayName,
      readingPoints: user.readingPoints,
      picture: user.picture,
      borrowedBooks: user.borrowedBooks
      ? user.borrowedBooks.map(book => this.toReturnBookDTO(book))
      : undefined,
      banEndDate: user.banEndDate
      ? user.banEndDate 
      : null,

    }
  }

  toReturnBookDTO(book: Book): ReturnBookDTO {
    return book ?
      {
        id: book.id,
        title: book.title,
        author: book.author,
        description: book.description,
        picture: book.picture,
        link: book.link,
        rating: book.rating,
        bookStatus: book.bookStatus,
        borrowedBy: book.borrowedBy,
        bookReviews: book.bookReviews,
      } :
      null
  }

  toReturnReviewDTO(review: Review): ReturnReviewDTO {
    return {
      id: review.id,
      content: review.content,
      date: review.date.toLocaleString('en-GB', { hour12: false }),
      createdBy: review.createdBy ?
        this.toReturnUserDTO(review.createdBy) :
        undefined,
      reviewBook: review.reviewBook ?
        this.toReturnBookDTO(review.reviewBook) :
        undefined,
      Likes: review.Likes,
      Dislikes: review.Dislikes,
    }
  }

  toReturnRateDTO(rate: BookRate): ReturnRateDTO {
    return {
      stars: rate.stars
    }
  }
}
