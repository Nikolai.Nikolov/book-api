import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { TransformService } from './transformer.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Review } from 'src/models/reviews.entity';
import { User } from 'src/models/user.entity';
import { Book } from 'src/models/books.entity';
import { Repository } from 'typeorm';
import { CreateReviewDTO } from 'src/dtos/review/create-review.dto';
import { UpdateReviewDTO } from 'src/dtos/review/update-review.dto';
import { ReviewLike } from 'src/models/review-like.entity';
import { ReviewReactionType } from 'src/enums/review-reaction-type';
import { UpdateReviewLike } from 'src/dtos/review/update-review-like.dto';
import { UserRole } from 'src/enums/user-role.enum';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';


@Injectable()
export class ReviewService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
        @InjectRepository(ReviewLike) private readonly reviewLikesRepository: Repository<ReviewLike>,
    ) { }

    async create(review: CreateReviewDTO, bookId: number, userId: number): Promise<ReturnMessageDTO> {

        const userFound = await this.findOneOrFailUser(userId);
        const bookFound = await this.findOneOrFailBook(bookId);

        if (review.content === null || review.content === undefined || review.content === '') {
            throw new BadRequestException('Content can\'t be an empty field');
        }

        userFound.readingPoints++;
        const createReview = await this.reviewsRepository.create(review);
        createReview.createdBy = userFound;
        createReview.reviewBook = bookFound;
        await this.reviewsRepository.save(createReview);
        await this.usersRepository.update(userId, userFound);
        await this.transformer.toReturnReviewDTO(createReview);

        return { message: 'Review created!' };

    }

    async update(review: UpdateReviewDTO, reviewId: number, userId: number): Promise<ReturnMessageDTO> {
        const userFound = await this.findOneOrFailUser(userId);
        const reviewFound = await this.reviewsRepository.findOne(reviewId, { relations: ['createdBy'] });

        if (!reviewFound) {
            throw new NotFoundException('Review with this ID does not exist');
        }

        const reviewUserId = reviewFound.createdBy.id;
        if (reviewUserId !== userId && userFound.role !== UserRole.Admin) {
            throw new BadRequestException('This review can\'t be edited by this user');
        }
        if (userFound.role === UserRole.Admin || reviewUserId === userFound.id) {
            await this.reviewsRepository.update(reviewId, review);
            await this.transformer.toReturnReviewDTO(reviewFound);

            return { message: 'Review updated!' };
        }

    }

    async delete(reviewId: number, userId: number): Promise<ReturnMessageDTO> {
        const userFound = await this.findOneOrFailUser(userId);
        const reviewFound = await this.reviewsRepository.findOne(reviewId, { relations: ['createdBy'] });

        if (!reviewFound) {
            throw new NotFoundException('Review with this ID does not exist');
        }

        if (reviewFound.createdBy.id !== userId && userFound.role !== UserRole.Admin) {
            throw new BadRequestException('This review can\'t be deleted by this user');
        }

        if (userFound.role === UserRole.Admin || reviewFound.createdBy.id === userFound.id) {

            reviewFound.isDeleted = true;
            await this.reviewsRepository.save(reviewFound);

            return { message: `Review with id ${reviewFound.id} is deleted!` }
        }
    }

    async giveVote(reviewLike: UpdateReviewLike, reviewId: number, userId: number): Promise<ReturnMessageDTO> {
        const foundReview = await this.reviewsRepository.findOne(reviewId, { where: { isDeleted: false }, relations: ['createdBy'] });
        if (!foundReview) {
            throw new NotFoundException('This review is not existing to be voted upon')
        }

        const userReview = foundReview.createdBy;
        const userFound = await this.usersRepository.findOne(userId, { relations: ['reactionsReviews', 'reactionsReviews.review'] });
        const foundReviewLike = await this.reviewLikesRepository.findOne({ where: { review: foundReview, user: userFound }, relations: ['review', 'user'] });

        if (userFound.reactionsReviews.length !== 0) {
            const ifVoted = userFound.reactionsReviews.some(reactionReview => reactionReview.review.id === reviewId)

            if (ifVoted && foundReviewLike.reaction === reviewLike.reaction) {
                throw new BadRequestException('You can\'t vote twice');
            }
        }

        if (!foundReviewLike) {
            const newReaction = await this.reviewLikesRepository.create();
            newReaction.review = foundReview;
            newReaction.user = userFound;
            newReaction.reaction = reviewLike.reaction;
            await this.reviewLikesRepository.save(newReaction);
        } else {
            foundReviewLike.reaction = reviewLike.reaction;
            await this.reviewLikesRepository.update(foundReviewLike.id, foundReviewLike);
        }

        await this.countReactions(reviewId);
        await this.usersRepository.update(foundReview.createdBy.id, userReview);

        return { message: 'You have just voted!' };
    }

    private async countReactions(reviewID: number): Promise<void> {
        const foundReview = await this.reviewsRepository.findOne(reviewID);

        const foundReactionsLikes = await this.reviewLikesRepository
            .find({ where: { review: foundReview, reaction: ReviewReactionType.LIKE } });

        const foundReactionsDislikes = await this.reviewLikesRepository
            .find({ where: { review: foundReview, reaction: ReviewReactionType.DISLIKE } });

        const sumLikes = await foundReactionsLikes.reduce((sum: number, currentReaction) => sum + currentReaction.reaction, 0);
        const sumDislikes = await foundReactionsDislikes.reduce((sum: number, currentReaction) => sum + currentReaction.reaction, 0)

        foundReview.Likes = sumLikes;
        foundReview.Dislikes = sumDislikes / 2;

        await this.reviewsRepository.update(reviewID, foundReview);
    }

    private async findOneOrFailBook(bookId: number): Promise<Book> {
        const book = await this.booksRepository.findOne(bookId);

        if (!book) {
            throw new NotFoundException('No book!')
        }

        return book;
    }

    private async findOneOrFailUser(userId: number): Promise<User> {
        const user = await this.usersRepository.findOne(userId);

        if (!user) {
            throw new Error('No user!')
        }

        return user;
    }

}
