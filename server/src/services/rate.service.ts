import { ReturnBookDTO } from 'src/dtos/book/return-book.dto';
import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { TransformService } from './transformer.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/models/user.entity';
import { Book } from 'src/models/books.entity';
import { BookRate } from 'src/models/book-rate.entity';
import { CreateRateDTO } from 'src/dtos/book-rate/create-book-rate.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';

@Injectable()
export class RateService {
    constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
        @InjectRepository(BookRate) private readonly booksRateRepository: Repository<BookRate>,

    ) { }

    async create(rating: CreateRateDTO, bookId: number, userId: number): Promise<ReturnBookDTO> {
        const foundUser = await this.usersRepository.findOne(userId);
        if (!foundUser) {
            throw new NotFoundException('No user found');
        }

        const bookFound = await this.booksRepository.findOne(bookId);
        if (!bookFound) {
            throw new NotFoundException('no such book');
        }

        if (rating.stars === null || rating.stars === undefined) {
            throw new BadRequestException('Stars can\'t be an empty field');
        }

        if (await this.booksRateRepository.findOne({ where: { ratedBy: foundUser, books: bookFound } })) {
            throw new BadRequestException('You can\'t double rate a book');
        }
        const createRate = await this.booksRateRepository.create(rating);
        createRate.books = bookFound;
        createRate.ratedBy = foundUser;
        await this.booksRateRepository.save(createRate);
        const bookWithNewRating = await this.countRating(bookId);

        return bookWithNewRating;
    }

    private async countRating(bookId: number): Promise<ReturnBookDTO> {

        const foundBook = await this.booksRepository.findOne(bookId);
        const foundRating = await this.booksRateRepository.find({ where: { books: foundBook } });
        const sumRates = foundRating.reduce((sum: number, currentRating) => sum + currentRating.stars, 0) / foundRating.length;
        foundBook.rating = +sumRates.toFixed(2);
        await this.booksRepository.update(bookId, foundBook);
        return foundBook;
    }
}
