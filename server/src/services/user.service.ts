import { User } from 'src/models/user.entity';
import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDTO } from 'src/dtos/users/create-user.dto';
import { UpdateUserDTO } from 'src/dtos/users/update-user.dto';
import * as bcrypt from 'bcrypt';
import { UserRole } from 'src/enums/user-role.enum';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { TransformService } from './transformer.service';

@Injectable()
export class UsersService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,

    ) { }

    async getAll() {
        const users = await this.usersRepository.find({ 
            where: {isDeleted: false,
            banEndDate: null},
            relations: ['borrowedBooks', 'postedReviews', 'postedReviews.reviewBook'] 
        });
 
        const returnedUsers =  users.map(user => this.transformer.toReturnUserDTO(user));
        const sortedUsers = returnedUsers.sort((a, b) => b.readingPoints - a.readingPoints)
        const topUsers = sortedUsers.slice(0, 5)
        return topUsers

    }

    async getById(userId: number): Promise<ReturnUserDTO> {
        const user = await this.usersRepository.findOne(
            { 
                where: {
                    id: userId,
                    isDeleted: false,
                },
                relations: ['borrowedBooks', 'postedReviews', 'postedReviews.reviewBook'] 
            });
        if (!user) {
            throw new BadRequestException('No such user');
        }
        return this.transformer.toReturnUserDTO(user);
    }

    async create(userDTO: CreateUserDTO): Promise<ReturnMessageDTO> {

        const user = this.usersRepository.create(userDTO);
        user.password = await bcrypt.hash(user.password, 10);
        await this.usersRepository.save(user);

        return { message: 'User created' };
    }

    async update(userDTO: UpdateUserDTO, loggedUser: number, userId: number): Promise<ReturnMessageDTO> {
        const user = await this.usersRepository.findOneOrFail(userId);
        if (!user) {
            throw new NotFoundException('no such user')
        }
        const foundLoggedUserRole = await (await this.usersRepository.findOneOrFail(loggedUser)).role
        if (loggedUser === userId || foundLoggedUserRole === UserRole.Admin) {
            user.displayName = userDTO.displayName;
            await this.usersRepository.update(userId, user);
        } else {
            throw new BadRequestException('User can\'t be update by the requesting user');
        }

        return { message: 'User updated' };
    }

}
