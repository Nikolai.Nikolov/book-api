import { Length, IsString } from 'class-validator';

export class CreateUserDTO {
  @IsString()
  @Length(4, 20)
  username: string;

  @IsString()
  @Length(4, 20)
  password: string;

  @IsString()
  @Length(4, 20)
  displayName: string;
}
