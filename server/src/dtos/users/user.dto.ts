import { ReturnBookDTO } from './../book/return-book.dto';
import { BookDTO } from '../book/book-dto';
import { ReviewDTO } from '../review/review.dto';

export class UserDTO {
    id: number;
    username: string;
    password: string;
    displayName: string;
    borrowedBooks: ReturnBookDTO[];
    postedReviews: ReviewDTO[];
    readingPoints: number;
    picture: string;
    ratedBooks: BookDTO[];
    isDeleted: boolean;
}
