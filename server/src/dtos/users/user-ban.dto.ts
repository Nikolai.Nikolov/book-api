import { IsNumber, IsPositive, IsString, IsNotEmpty } from 'class-validator';

export class UserBanDTO {
  @IsNumber()
  @IsPositive()
  period: number;
  @IsString()
  @IsNotEmpty()
  description: string;
}
