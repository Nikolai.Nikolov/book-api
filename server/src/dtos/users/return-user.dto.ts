import { ReturnBookDTO } from './../book/return-book.dto';

export class ReturnUserDTO {
  id: number;
  username: string;
  displayName: string;
  readingPoints: number;
  picture: string;
  borrowedBooks: ReturnBookDTO[];
  banEndDate: Date;
}
