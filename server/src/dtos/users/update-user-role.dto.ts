import { UserRole } from 'src/enums/user-role.enum';

export class UpdateUserRoleDTO {
    role: UserRole;
}
