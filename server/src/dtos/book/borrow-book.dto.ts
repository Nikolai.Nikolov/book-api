import { BookStatus } from './../../enums/book-status';
import { UserDTO } from '../users/user.dto';
import { BookDTO } from './book-dto';
import { IsEnum } from 'class-validator';

export class BorrowBookDTO {

    @IsEnum(BookStatus)
    bookStatus: BookStatus;
    public bookId: BookDTO;
    public borrowedBy: UserDTO;
}
