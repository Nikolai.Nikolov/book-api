import { BookStatus } from 'src/enums/book-status';

export class FindBookDTO {
  id?: number;
  title?: string;
  author?: string;
  description?: string;
  bookStatus?: BookStatus;
}
