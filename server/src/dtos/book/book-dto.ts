export class BookDTO {
    id: number;
    title: string;
    author: string;
    description: string;
    picture: string; 
    link: string;
    rating: number;
}
