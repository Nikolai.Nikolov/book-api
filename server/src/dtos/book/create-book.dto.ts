import { BookStatus } from 'src/enums/book-status';
import { IsString, MaxLength, Length, IsNotEmpty, IsUrl } from 'class-validator';

export class CreateBookDTO {
    bookStatus: BookStatus;

    @IsString()
    @MaxLength(200)
    @IsNotEmpty()
    title: string;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    author: string;

    @IsString()
    @IsNotEmpty()
    @Length(0, 1000)
    description: string;

    // @IsNotEmpty()
    // @IsUrl()
    // picture: string;

    // @IsNotEmpty()
    // @IsUrl()
    // link: string;
}
