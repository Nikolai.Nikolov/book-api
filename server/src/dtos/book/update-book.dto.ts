export class UpdateBookDTO {
    title?: string;
    author?: string;
    description?: string;
}
