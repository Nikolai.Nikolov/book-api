import { BookStatus } from "src/enums/book-status";
import { ReturnUserDTO } from "../users/return-user.dto";
import { ReturnReviewDTO } from "../review/return-review.dto";

export class ReturnBookDTO {
    id:number;
    title: string;
    author: string;
    description: string;
    picture: string;
    link: string;
    rating: number;
    bookStatus: BookStatus;
    borrowedBy: ReturnUserDTO;
    bookReviews: ReturnReviewDTO[];
}
