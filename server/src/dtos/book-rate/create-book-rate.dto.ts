import { Min, Max, IsInt } from 'class-validator';
export class CreateRateDTO {
    @IsInt()
    @Min(1)
    @Max(5)
    stars: number;
}
