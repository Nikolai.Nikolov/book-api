import { ReviewReactionType } from 'src/enums/review-reaction-type';
import { ReturnReviewDTO } from '../review/return-review.dto';

export class ReturnReviewLikeDTO {
    id: number;
    reaction: ReviewReactionType;
    review: ReturnReviewDTO;
}
