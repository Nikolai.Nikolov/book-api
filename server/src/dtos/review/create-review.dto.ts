import { Length, IsString } from 'class-validator';
export class CreateReviewDTO {
    @IsString()
    @Length(1, 200)
    content: string;
}
