import { ReturnBookDTO } from '../book/return-book.dto';
import { ReturnUserDTO } from '../users/return-user.dto';

export class ReturnReviewDTO {
        id: number;
        content: string;
        createdBy: ReturnUserDTO;
        date: string;
        reviewBook: ReturnBookDTO;
        Likes: number;
        Dislikes: number;
}
