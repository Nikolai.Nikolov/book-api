import { IsString, Length } from 'class-validator';

export class UpdateReviewDTO {
    @IsString()
    @Length(1, 200)
    content: string;
}
