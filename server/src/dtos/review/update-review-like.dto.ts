import { ReviewReactionType } from 'src/enums/review-reaction-type';
import { IsEnum } from 'class-validator';

export class UpdateReviewLike {
    @IsEnum(ReviewReactionType)
    reaction: ReviewReactionType;
}
