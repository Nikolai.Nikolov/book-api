import { ReturnBookDTO } from '../book/return-book.dto';
import { ReturnUserDTO } from '../users/return-user.dto';

export class ReviewDTO {
    content: string;
    createdBy: ReturnUserDTO;
    date: Date;
    reviewBook: ReturnBookDTO;
    Likes: number;
    Dislikes: number;
}
