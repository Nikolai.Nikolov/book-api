import { Controller, Post, Body, HttpCode, HttpStatus, Put, UseGuards, Param, Get } from '@nestjs/common';
import { CreateUserDTO } from 'src/dtos/users/create-user.dto';
import { UsersService } from 'src/services/user.service';
import { UpdateUserDTO } from 'src/dtos/users/update-user.dto';
import { UserId } from 'src/auth/user-id.decorator';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';

@Controller('users')
export class UsersController {
  public constructor(private readonly usersService: UsersService) { }

  @Get()
  @UseGuards(AuthGuard('jwt'),  BlacklistGuard)
  async allUsers(): Promise<ReturnUserDTO[]> {
    const users = await this.usersService.getAll();
    
    return users;
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async getById(@Param('id') userID: string): Promise<ReturnUserDTO> {
    const user = await this.usersService.getById(+userID);

    return user;
  }

  @Post()
  public async addNewUser(@Body() user: CreateUserDTO): Promise<ReturnMessageDTO> {

    return this.usersService.create(user);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard)
  @HttpCode(HttpStatus.OK)
  async updateUser(@Body() userdto: UpdateUserDTO, @UserId() loggedUser: number, @Param('id')  userId: string): Promise<ReturnMessageDTO> {

    return await this.usersService.update(userdto, loggedUser, +userId);
  }
}
