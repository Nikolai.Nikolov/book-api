import { AuthController } from './auth.controllet';
import { UsersController } from './users.controller';
import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/services.module';
import { BooksController } from './books.controller';
import { ReviewController } from './reviews.controller';
import { AdminController } from './admin.controller';

@Module({
    imports: [ ServicesModule ],
    controllers: [ UsersController, BooksController, ReviewController, AuthController, AdminController ]
})
export class ControllersModule { }
