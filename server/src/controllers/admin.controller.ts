import { Controller, Put, UseGuards, Body, Param, Delete, Post, Get, HttpCode, HttpStatus, ParseIntPipe, ValidationPipe } from '@nestjs/common';
import { AdminService } from 'src/services/admin.service';
import { UserRole } from 'src/enums/user-role.enum';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/roles.guard';
import { UpdateUserRoleDTO } from 'src/dtos/users/update-user-role.dto';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { ReturnReviewDTO } from 'src/dtos/review/return-review.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { UserBanDTO } from 'src/dtos/users/user-ban.dto';

@Controller('admin')
export class AdminController {
    constructor(
        private readonly adminService: AdminService,

    ) { }

    @Get('users')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    @HttpCode(HttpStatus.OK)
    async allUsers(): Promise<ReturnUserDTO[]> {
        const users = await this.adminService.getAll();

        return users;
    }

    @Get('users/:id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    @HttpCode(HttpStatus.OK)
    async getById(@Param('id') userId: number): Promise<ReturnUserDTO> {
        const user = await this.adminService.getById(userId);

        return user;
    }

    @Get('users/:id/reviews')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    @HttpCode(HttpStatus.OK)
    async getUserReviews(@Param('id') userId: string): Promise<ReturnReviewDTO[]> {

        return await this.adminService.getUserReviews(+userId);
    }

    @Put('users/:userId')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async changeRole(@Body() user: UpdateUserRoleDTO, @Param('userId') userId: string): Promise<ReturnMessageDTO> {

        return await this.adminService.changeRole(user, +userId);
    }

    @Delete('users/:id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async deleteUser(@Param('id') userId: number): Promise<ReturnMessageDTO> {

        return await this.adminService.deleteUser(userId);
    }

    @Post('users/:id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async restoreUser(@Param('id') userId: number): Promise<ReturnMessageDTO> {

        return await this.adminService.restoreUser(userId);
    }

    @Post('users/:id/ban')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    async banUser(@Param('id', ParseIntPipe) userId: number, @Body(new ValidationPipe({ whitelist: true })) @Body() banUser: UserBanDTO)
        : Promise<ReturnMessageDTO> {
        return await this.adminService.banUser(userId, banUser.period, banUser);
    }
}
