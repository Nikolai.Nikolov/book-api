import { Controller, Post, Body, Param, Put, Delete, UseGuards } from '@nestjs/common';
import { ReviewService } from 'src/services/review.service';
import { CreateReviewDTO } from 'src/dtos/review/create-review.dto';
import { UserId } from 'src/auth/user-id.decorator';
import { UpdateReviewDTO } from 'src/dtos/review/update-review.dto';
import { UpdateReviewLike } from 'src/dtos/review/update-review-like.dto';
import { AuthGuard } from '@nestjs/passport';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';

@Controller('reviews')

export class ReviewController {
  public constructor(private readonly reviewService: ReviewService) { }

  @Post('books/:bookId')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard)
  async create(@Body() review: CreateReviewDTO, @Param('bookId') bookId: string, @UserId() userId: number): Promise<ReturnMessageDTO> {

    return await this.reviewService.create(review, +bookId, userId);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard)
  async update(@Body() review: UpdateReviewDTO, @Param('id') reviewId: string, @UserId() userId: number): Promise<ReturnMessageDTO> {

    return await this.reviewService.update(review, +reviewId, userId);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard)
  async delete(@Param('id') reviewId: string, @UserId() userId: number): Promise<{ message: string }> {

    return await this.reviewService.delete(+reviewId, userId);
  }

  @Put(':id/votes')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard)
  async giveVote(@Body() reviewLike: UpdateReviewLike, @Param('id') reviewId: string, @UserId() userId: number): Promise<ReturnMessageDTO> {
    return await this.reviewService.giveVote(reviewLike, +reviewId, userId)
  }
}
