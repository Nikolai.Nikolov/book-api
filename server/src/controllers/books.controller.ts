import { ReturnBookDTO } from 'src/dtos/book/return-book.dto';
import { Controller, Get, Query, BadRequestException, Post, Body, Put, Param, UseGuards, Delete, Patch } from '@nestjs/common';
import { BooksService } from 'src/services/books.service';
import { FindBookDTO } from 'src/dtos/book/find-book.dto';
import { CreateBookDTO } from 'src/dtos/book/create-book.dto';
import { BookStatus } from 'src/enums/book-status';
import { UpdateBookDTO } from 'src/dtos/book/update-book.dto';
import { ReturnReviewDTO } from 'src/dtos/review/return-review.dto';
import { CreateRateDTO } from 'src/dtos/book-rate/create-book-rate.dto';
import { RateService } from 'src/services/rate.service';
import { UserId } from 'src/auth/user-id.decorator';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enums/user-role.enum';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';

@Controller('books')
export class BooksController {
    public constructor(
        private readonly booksService: BooksService,
        private readonly bookRateService: RateService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async all(@Query() query: FindBookDTO): Promise<ReturnBookDTO[]> {

        if (Object.keys(query).length) {
            const options = this.toFindBookDTO({
                id: query.id,
                title: query.title,
                author: query.author,
                description: query.description,
                bookStatus: query.bookStatus,
            });

            return await this.booksService.findBook(options);
        }

        return await this.booksService.getAll();
    }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    public async findBookById(@Param('id') bookId: string): Promise<ReturnBookDTO> {
        
        return await this.booksService.getBookById(+bookId);
    }

    @Get(':id/reviews')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    public async getBookReviews(@Param('id') bookId: string, @UserId() loggedUserId: number): Promise<ReturnReviewDTO[]> {

        return await this.booksService.getBookReviews(+bookId, loggedUserId);
    }

    @Post()
    @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Admin))
    public async addNewBook(@Body() book: CreateBookDTO): Promise<ReturnMessageDTO> {

        return await this.booksService.create(book);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async borrowBook(@Param('id') bookId: string, @UserId() loggedUserId: number): Promise<ReturnMessageDTO> {

        return await this.booksService.borrowBook(+bookId, loggedUserId);
    }

    @Delete(':bookId')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async returnBook(@Param('bookId') bookId: string, @UserId() loggedUserId: number): Promise<ReturnMessageDTO> {

        return await this.booksService.returnBook(+bookId, loggedUserId);
    }

    @Patch(':id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async updateBook(@Param('id') bookId: string, @Body() bookDTO: UpdateBookDTO): Promise<ReturnMessageDTO> {

        return await this.booksService.updateBookDetails(bookDTO, +bookId);
    }

    private toFindBookDTO(source: any): FindBookDTO {

        const keys = ['id', 'title', 'author', 'description'];

        if (source.id && typeof source.id !== 'string') {
            throw new BadRequestException(`Invalid id!`);
        }
        if (source.title && typeof source.title !== 'string') {
            throw new BadRequestException(`Invalid book title!`);
        }
        if (source.author && typeof source.author !== 'string') {
            throw new BadRequestException(`Invalid author!`);
        }
        if (source.description && typeof source.description !== 'string') {
            throw new BadRequestException(`Invalid description!`);
        }
        if (source.bookStatus && !Object.values(BookStatus).includes(source.bookStatus)) {
            throw new BadRequestException(`Invalid status!`);
        }

        keys.forEach((key: string) => {
            if (!source[key]) {
                delete source[key];
            }
        });

        Object.keys(source).forEach((key: string) => {
            if (!keys.includes(key)) {
                delete source[key];
            }
        });

        return source;
    }

    @Delete(':id/unlisted')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async delete(@Param('id') bookID: string): Promise<{ message: string }> {
        await this.booksService.delete(+bookID);

        return {
            message: `Book deleted!`,
        };
    }

    @Post(':id/retrieved')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async retrieveBook(@Param('id') bookID: string): Promise<{ message: string }> {
        await this.booksService.retrieve(+bookID);

        return {
            message: `Book retrieved!`,
        };
    }

    @Post(':bookId/rating')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)

    public async addBookRate(@Body() rating: CreateRateDTO, @Param('bookId') bookId: string, @UserId() userId: number): Promise<ReturnBookDTO> {
       
        return await this.bookRateService.create(rating, +bookId, userId);
    }

}
